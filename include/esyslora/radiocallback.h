/*!
 * \file esyslora/radiocallback.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"

#include <inttypes.h>

namespace esyslora
{

class ESYSLORA_API RadioBase;

/*! \class RadioBase esyslora/radiocallback.h "esyslora/radiocallback.h"
 *  \brief
 *
 */
class ESYSLORA_API RadioCallback
{
public:
    enum class Event
    {
        TX_DONE,
        TX_TIMEOUT,
        RX_TIMEOUT,
        RX_ERROR,
    };

    //! Constructor
    RadioCallback();

    //! Destructor
    virtual ~RadioCallback();

    //! Notification sent by the radio
    /*!
     * \param[in] radio the radio which sends the notification
     * \param[in] evt the event being notified

     */
    virtual void event(RadioBase *radio, Event evt) = 0;

    //! Notification when a message was received
    /*!
     * \param[in] radio the radio which sends the notification
     * \param[in] data the buffer with the
     * message received
     * \param[in] size the size of the received message
     * \param[in] rssi the RSSI
     * \param[in] snr the SNR
     */
    virtual void rx_done(RadioBase *radio, uint8_t *data, uint32_t size, int16_t rssi, int8_t snr) = 0;

    //! Notification of a frequency hoping change
    /*!
     * \param[in] radio the radio which sends the notification
     * \param[in] current_channel the current
     * channel
     */
    virtual void freq_hoping_change(RadioBase *radio, uint8_t current_channel) = 0;

    //! Notification that a channel activity detection was completed
    /*!
     * \param[in] radio the radio which sends the notification
     * \param[in] activity_detected true if
     * detected, false otherwise
     */
    virtual void channel_activity_detection_done(RadioBase *radio, bool activity_detected) = 0;
};

} // namespace esyslora
