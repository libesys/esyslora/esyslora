/*!
 * \file esyslora/radiobase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/modulation.h"
#include "esyslora/loracfg.h"
#include "esyslora/radiocallback.h"

namespace esyslora
{

/*! \class RadioBase esyslora/radiobase.h "esyslora/radiobase.h"
 *  \brief
 *
 */
class ESYSLORA_API RadioBase
{
public:
    enum class Status
    {
        IDLE,
        RX_RUNNING,
        TX_RUNNING,
        CHANNEL_ACTIVITY_DETECTION
    };

    //! Constructor
    RadioBase();

    //! Destructor
    virtual ~RadioBase();

    //! Set the modulation that the radio should use
    /*!
     * \param[in] modulation the modulation of the radio
     */
    virtual void set_modulation(Modulation modulation) = 0;

    //! Get the modulation that the radio should use
    /*!
     * \return the modulation of the radio
     */
    virtual Modulation get_modulation() const = 0;

    //! Set the LoRa configuration of the radio
    /*!
     * \param[in] lora_cfg the LoRa configuration of the radio
     */
    virtual int set_lora_cfg(const LoRaCfg &lora_cfg) = 0;

    //! Get the LoRa configuration of the radio
    /*!
     * \return the LoRa configuration of the radio
     */
    virtual const LoRaCfg &get_lora_cfg() const = 0;

    //! Get the LoRa configuration of the radio
    /*!
     * \return the LoRa configuration of the radio
     */
    virtual LoRaCfg &get_lora_cfg() = 0;

    //! Set the transmission power
    /*!
     * \param[in] power the transmission power
     */
    virtual void set_tx_power(int8_t power) = 0;

    //! Get the transmission power
    /*!
     * \return the transmission power
     */
    virtual int8_t get_tx_power() = 0;

    //! Get the status of the radio
    /*!
     * \return the status of the radio
     */
    virtual Status get_status() const = 0;

    //! Set the frequency the radio will use
    /*!
     * \param[in] frequency the frequency in Hz
     */
    virtual void set_frequency(double frequency) = 0;

    //! Get the frequency the radio will use
    /*!
     * \return the frequency in Hz
     */
    virtual double get_frequency() const = 0;

    virtual bool is_channel_free(int16_t rssi_threshold, uint32_t max_carrier_sense_time) = 0;

    //! Transmit a message
    /*!
     * \param[in] msg the message to transmit
     * \param[in] size the size of the message
     * \return 0 if
     * success, < 0 otherwise
     */
    virtual int tx(uint8_t *msg, uint32_t size) = 0;

    //! Set the Radio is receiving mode
    /*!
     * \param[in] timeout the time the receiving mode should be enabled
     * \return 0 if success, < 0
     * otherwise
     */
    virtual int rx(uint32_t timeout) = 0;

    //! Start the activity detection
    /*!
     */
    virtual void start_channel_activity_detection() = 0;

    //! Get the least measured RSSI
    /*!
     * \return the RSSI
     */
    virtual int16_t get_rssi() = 0;

    //! Get the least measured RSSI
    /*!
     */
    virtual uint32_t get_wake_up_time() = 0;

    //! Set the callback
    /*!
     * \param[in] callback the callback
     */
    void set_callback(RadioCallback *callback);

    //! Get the callback
    /*!
     * \return the callback
     */
    RadioCallback *get_callback() const;

    //! Set the ID of the radio
    /*!
     * \param[in] id the ID of the radio
     */
    void set_id(int id);

    //! Get the ID of the radio
    /*!
     * \return the ID of the radio
     */
    int get_id() const;

    void set_channel(uint32_t frequency);
    void set_tx_config(Modulation modem, int8_t power, uint32_t fdev, uint32_t bandwidth, uint32_t datarate,
                       uint8_t coderate, uint16_t preambleLen, bool fixLen, bool crcOn, bool freqHopOn,
                       uint8_t hopPeriod, bool iqInverted, uint32_t timeout);
    void set_rx_config(Modulation modem, uint32_t bandwidth, uint32_t datarate, uint8_t coderate, uint32_t bandwidthAfc,
                       uint16_t preambleLen, uint16_t symbTimeout, bool fixLen, uint8_t payloadLen, bool crcOn,
                       bool freqHopOn, uint8_t hopPeriod, bool iqInverted, bool rxContinuous);
    void set_max_payload_length(Modulation modem, uint8_t max);
    void set_tx_continuous_wave(uint32_t freq, int8_t power, uint16_t time);
    uint32_t time_on_air(Modulation modem, uint32_t bandwidth, uint32_t datarate, uint8_t coderate, uint16_t preambleLen,
                       bool fixLen, uint8_t payloadLen, bool crcOn);
    bool check_rf_frequency(uint32_t frequency);
    void sleep();
    void standby();
    void set_public_network(bool enable);
    uint32_t random();

protected:
    //!< \cond DOXY_IMPL
    RadioCallback *m_callback = nullptr; //!< The callback
    int m_id = -1;                       //!< The ID of the radio
    //!< \endcond
};

} // namespace esyslora
