/*!
 * \file esyslora/loracfg.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsgbase.h"
#include "esyslora/bandwidth.h"
#include "esyslora/errorcodingrate.h"
#include "esyslora/headermode.h"
#include "esyslora/spreadingfactor.h"

#include <ostream>

namespace esyslora
{

/*! \class LoRaCfg esyslora/loracfg.h "esyslora/loracfg.h"
 *  \brief
 *
 */
class ESYSLORA_API LoRaCfg
{
public:
    //! Default constructor
    LoRaCfg();

    LoRaCfg(Bandwidth bandwidth, ErrorCodingRate error_coding_rate, SpreadingFactor spreading_factor);

    //! Destructor
    virtual ~LoRaCfg();

    //! Set bandwidth used to send the data in a LoRa packet
    /*!
     * \param[in] bandwidth the bandwidth
     */
    void set_bandwidth(Bandwidth bandwidth);

    //! Get bandwidth used to send the data in a LoRa packet
    /*!
     * \result the bandwidth
     */
    Bandwidth get_bandwidth() const;

    //! Set the error coding rate of the packet
    /*!
     * \param[in] error_coding_rate the error coding rate
     */
    void set_error_coding_rate(ErrorCodingRate error_coding_rate);

    //! Get the error coding rate of the packet
    /*!
     * \return the error coding rate
     */
    ErrorCodingRate get_error_coding_rate() const;

    //! Set the header mode used to sent the LoRa packet
    /*!
     * \param[in] header_mode the header mode
     */
    void set_header_mode(HeaderMode header_mode);

    //! Get the header mode used to sent the LoRa packet
    /*!
     * \return the header mode
     */
    HeaderMode get_header_mode() const;

    //! Set the spreading factor of the LoRa packet
    /*!
     * \param[in] spreading_factor the spreading factor
     */
    void set_spreading_factor(SpreadingFactor spreading_factor);

    //! Get the spreading factor of the LoRa packet
    /*!
     * \return the spreading factor
     */
    SpreadingFactor get_spreading_factor() const;

    //! Set the preamble size in symbols of the LoRa packet
    /*!
     * \param[in] preamble_size the preamble size in symbols
     */
    void set_preamble_size(std::size_t preamble_size);

    //! Get the preamble size in symbols of the LoRa packet
    /*!
     * \return the preamble size in symbols
     */
    std::size_t get_preamble_size() const;

    //! Set if CRC is used or not in the LoRa packet
    /*!
     * \param[in] use_crc true, if CRC is used; false otherwise
     */
    void set_use_crc(bool use_crc = true);

    //! Get if CRC is used or not in the LoRa packet
    /*!
     * \return true, if CRC is used false otherwise
     */
    bool get_use_crc() const;

    //! Set if low data rate optimization is used when sending the LoRa packet
    /*!
     * \param[in] use_low_data_rate_optimization true, if low data rate optimization is used; false otherwise
     */
    void set_use_low_data_rate_optimization(bool use_low_data_rate_optimization = true);

    //! Get if low data rate optimization is used when sending the LoRa packet
    /*!
     * \return true, if low data rate optimization is used; false otherwise
     */
    bool get_use_low_data_rate_optimization() const;

    //! Set the frequency at which the LoRa packet was sent
    /*!
     * \param[in] frequency the frequency in Hz
     */
    void set_frequency(double frequency);

    //! Get the frequency at which the LoRa packet was sent
    /*!
     * \return the frequency in Hz
     */
    double get_frequency() const;

protected:
    //!< \cond DOXY_IMPL
    Bandwidth m_bandwidth = Bandwidth::NOT_SET;                     //!< The bandwidth
    ErrorCodingRate m_error_coding_rate = ErrorCodingRate::NOT_SET; //!< The error coding rate
    HeaderMode m_header_mode = HeaderMode::EXPLICIT;                //!< The header mode
    SpreadingFactor m_spreading_factor = SpreadingFactor::NOT_SET;  //!< The spreading factor
    std::size_t m_preamble_size = 8;                                //!< The preamble size
    bool m_use_crc = true;                                          //!< If CRC is used or not
    bool m_use_low_data_rate_optimization = false; //!< If the low data rate optimization is used or not
    double m_frequency = 0.0;                      //!< The frequency
    //!< \endcond
};

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::LoRaCfg *lora_cfg);
ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::LoRaCfg &lora_cfg);
