/*!
 * \file esyslora/sim/channelport.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg_if.h"
#include "esyslora/sim/airmsgcb_if.h"
#include "esyslora/sim/channel_if.h"
#include "esyslora/sim/channelcb_if.h"
#include "esyslora/receivability.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class ChannelPort esyslora/sim/channelport.h "esyslora/sim/channelport.h"
 *  \brief
 *
 */
class ESYSLORA_API ChannelPort : public sc_module, public AirMsg_if, public ChannelCb_if
{
public:
    //! Constructor
    ChannelPort(const sc_module_name &name);

    //! Destructor
    virtual ~ChannelPort();

    // AirMsg_if
    virtual void am_tx_msg(std::shared_ptr<AirMsg> msg) override;
    virtual void am_rx(const LoRaCfg &lora_cfg) override;
    virtual void am_idle() override;

    // ChannelCb_if
    virtual void chcb_msg_start(std::shared_ptr<ChannelMsg> msg) override;
    virtual void chcb_msg_done(std::shared_ptr<ChannelMsg> msg) override;

    //! Set the ID of the ChannelPort
    /*!
     * \param[in] id the ID of the ChannelPort
     */
    void set_id(std::size_t id);

    //! Get the ID of the ChannelPort
    /*!
     * \return the ID of the ChannelPort
     */
    std::size_t get_id() const;

    //! Set the Channel interface
    /*!
     * \param[in] channel_if the Channel interface
     */
    void set_channel_if(Channel_if *channel_if);

    //! Get the Channel interface
    /*!
     * \return the Channel interface
     */
    Channel_if *get_channel_if() const;

    //! Set the AirMsg callback interface
    /*!
     * \param[in] air_msg_cb_if the AirMsg callback interface
     */
    void set_air_msg_cb_if(AirMsgCb_if *air_msg_cb_if);

    //! Get the AirMsg callback interface
    /*!
     * \return the AirMsg callback interface
     */
    AirMsgCb_if *get_air_msg_cb_if() const;

    //! Set the current ongoing message, which could be received
    /*!
     * \param[in] cur_msg the current ongoing message
     */
    void set_cur_msg(std::shared_ptr<ChannelMsg> cur_msg);

    //! Get the current ongoing message, which could be received
    /*!
     * \return the current ongoing message
     */
    std::shared_ptr<ChannelMsg> get_cur_msg();

    //! Get the current ongoing message, which could be received
    /*!
     * \return the current ongoing message
     */
    const std::shared_ptr<ChannelMsg> get_cur_msg() const;

    //! Get the object used to determine if a message can be received or not
    /*!
     * \return the object used to determine if a message can be received or not
     */
    Receivability &get_receivability();

    //! Get the LoRa configuration in effect at the moment
    /*!
     * \return the LoRa configuration in effect at the moment
     */
    const LoRaCfg *get_lora_cfg();

protected:
    //!< \cond DOXY_IMPL
    std::size_t m_id = 0;                   //!< The ID
    Channel_if *m_channel_if = nullptr;     //!< The Channel interface
    AirMsgCb_if *m_air_msg_cb_if = nullptr; //!< The AirMsg callback interface
    std::shared_ptr<ChannelMsg> m_cur_msg;  //!< The current ongoing receiveable message
    Receivability m_receivability;          //!< The object to known if a message can be received
    const LoRaCfg *m_lora_cfg = nullptr;    //!< The LoRa configuration is used to receive a message
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
