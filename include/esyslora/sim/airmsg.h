/*!
 * \file esyslora/sim/airmsg.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsgbase.h"
#include "esyslora/loracfg.h"

#include <ostream>

namespace esyslora
{

namespace sim
{

/*! \class AirMsg esyslora/sim/airmsg.h "esyslora/sim/airmsg.h"
 *  \brief LoRa message including metadata needed for simulation
 *
 */
class ESYSLORA_API AirMsg : public AirMsgBase
{
public:
    //! Constructor
    AirMsg();

    //! Destructor
    virtual ~AirMsg();

    //! Set the LoRa HW configuration used for this message
    /*!
     * \param[in] lora_cfg the LoRa HW configuration
     */
    void set_lora_cfg(const LoRaCfg &lora_cfg);

    //! Get the LoRa HW configuration used for this message
    /*!
     * \result the LoRa HW configuration
     */
    const LoRaCfg &get_lora_cfg() const;

    //! Get the LoRa HW configuration used for this message
    /*!
     * \result the LoRa HW configuration
     */
    LoRaCfg &get_lora_cfg();

    //! Set the RSSI (received signal strength indicator)
    /*!
     * \param[in] rssi the RSSI
     */
    void set_rssi(int16_t rssi);

    //! Get the RSSI
    /*!
     * \result the RSSI
     */
    int16_t get_rssi() const;

    //! Set the SNR (signal noise ratio)
    /*!
     * \param[in] snr the SNR
     */
    void set_snr(int8_t snr);

    //! Get the SNR (signal noise ratio)
    /*!
     * \return the SNR
     */
    int8_t get_snr() const;

    //! Get the ID of the message, which is unique
    /*!
     * \return the ID of the message
     */
    int64_t get_id() const;

protected:
    //!< \cond DOXY_IMPL
    static int64_t m_count; //!< The number of messages created so far

    int64_t m_id = -1;  //!< The ID of the message, the count value when created
    LoRaCfg m_lora_cfg; //!< The LoRa HW configuration used to sent this message
    int16_t m_rssi = 0; //!< The RSSI
    int8_t m_snr = 0;   //!< The SNR
    //!< \endcond
};

} // namespace sim

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::sim::AirMsg *air_msg);
ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::sim::AirMsg &air_msg);
