/*!
 * \file esyslora/sim/airmsgcb_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class AirMsgCb_if esyslora/sim/airmsgcb_if.h "esyslora/sim/airmsgcb_if.h"
 *  \brief
 *
 */
class ESYSLORA_API AirMsgCb_if
{
public:
    //! Constructor
    AirMsgCb_if();

    //! Destructor
    virtual ~AirMsgCb_if();

    //! A message was received
    /*!
     * \param[in] msg the received message
     */
    virtual void rx_msg(std::shared_ptr<AirMsg> msg) = 0;

    //! The transfer of the message is done
    /*!
     */
    virtual void tx_done() = 0;
};

} // namespace sim

} // namespace esyslora
