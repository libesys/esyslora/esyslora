/*!
 * \file esyslora/sim/radio.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg_if.h"
#include "esyslora/loracfg.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class Radio esyslora/sim/radio.h "esyslora/sim/radio.h"
 *  \brief
 *
 */
class ESYSLORA_API Radio : public sc_module, public AirMsgCb_if
{
public:
    //! Default constructor
    Radio();

    //! Constructor
    /*!
     * \param[in] name the name of the radio
     */
    Radio(const sc_module_name &name);

    //! Destructor
    virtual ~Radio();

    //! Set the number channels supported by the radio
    /*!
     * \param[in] channel_count the number of channels supported
     */
    void set_channel_count(std::size_t channel_count);

    //! Get the number channels supported by the radio
    /*!
     * \return the number of channels supported
     */
    std::size_t get_channel_count() const;

    //! Get the LoRa configuration of all supported channels
    /*!
     * \return the LoRa configuration of all supported channels
     */
    std::vector<LoRaCfg> &get_lora_cfgs();

    //! Get the LoRa configuration of all supported channels
    /*!
     * \return the LoRa configuration of all supported channels
     */
    const std::vector<LoRaCfg> &get_lora_cfgs() const;

    //! Set the AirMsg interface
    /*!
     * \param[in] air_msg_if the AirMsg interface
     */
    void set_air_msg_if(AirMsg_if *air_msg_if);

    //! Get the AirMsg interface
    /*!
     * \return the AirMsg interface
     */
    AirMsg_if *get_air_msg_if();

    // AirMsgCb_if
    virtual void rx_msg(std::shared_ptr<AirMsg> msg) override;

protected:
    //!< \cond DOXY_IMPL
    std::vector<LoRaCfg> m_lora_cfgs;  //!< List of LoRa configurations, one per channel
    AirMsg_if *m_air_msg_if = nullptr; //!< The AirMsg interface
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
