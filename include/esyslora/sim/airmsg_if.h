/*!
 * \file esyslora/sim/airmsg_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg.h"
#include "esyslora/sim/airmsgcb_if.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class AirMsg_if esyslora/sim/airmsg_if.h "esyslora/sim/airmsg_if.h"
 *  \brief
 *
 */
class ESYSLORA_API AirMsg_if
{
public:
    //! Constructor
    AirMsg_if();

    //! Destructor
    virtual ~AirMsg_if();

    //! Transmit a message 
    /*!
     * \param[in] msg the message to transmit
     */
    virtual void am_tx_msg(std::shared_ptr<AirMsg> msg) = 0;

    //! The radio was setup to receive a message with a given LoRa HW configuration
    /*!
     * \param[in] lora_cfg the LoRa configuration
     */
    virtual void am_rx(const LoRaCfg &lora_cfg) = 0;

    //! The radio was setup to idle moden
    /*!
     */
    virtual void am_idle() = 0;

    //! Set the callback interface to use
    /*!
     * \param[in] cb_if the callback interface
     */
    void am_set_cb_if(AirMsgCb_if *cb_if);

    //! Get the callback interface to use
    /*!
     * \return the callback interface
     */
    AirMsgCb_if *am_get_cb_if();

protected:
    //!< \cond DOXY_IMPL
    AirMsgCb_if *m_cb_if = nullptr; //!< the callback interface
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
