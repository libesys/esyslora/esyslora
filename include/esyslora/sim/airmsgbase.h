/*!
 * \file esyslora/sim/airmsgbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/modulation.h"

#include <systemc.h>

#include <vector>

namespace esyslora
{

namespace sim
{

/*! \class AirMsgBase esyslora/sim/airmsgbase.h "esyslora/sim/airmsgbase.h"
 *  \brief Base class for message send over the air
 *
 */
class ESYSLORA_API AirMsgBase
{
public:
    //! Constructor
    AirMsgBase();

    //! Destructor
    virtual ~AirMsgBase();

    //! Set the modulation used to send the packet
    /*!
     * \param[in] modulation the modulation
     */
    void set_modulation(Modulation modulation);

    //! Get the modulation used to send the packet
    /*!
     * \return the modulation
     */
    Modulation get_modulation() const;

    //! Set the packet, the data sent over the air
    /*!
     * \param[in] data the packet sent over the air 
     */
    void set_data(const std::vector<uint8_t> &data);

    //! Get the packet, the data sent over the air
    /*!
     * \return the packet sent over the air
     */
    const std::vector<uint8_t> &get_data() const;

    //! Get the packet, the data sent over the air
    /*!
     * \return the packet sent over the air
     */
    std::vector<uint8_t> &get_data();

    //! Set the size of the packet, the data sent over the air
    /*!
     * \param[in] size the size packet sent over the air
     */
    void set_size(std::size_t size);

    //! Get the size of the packet, the data sent over the air
    /*!
     * \return the size packet sent over the air
     */
    std::size_t get_size() const;

    //! Set the time at which the packet was sent
    /*!
     * \param[in] sent_time the time at which the packet was sentr
     */
    void set_sent_time(const sc_time &sent_time);

    //! Get the time at which the packet was sent
    /*!
     * \return the time at which the packet was sentr
     */
    const sc_time &get_sent_time() const;
protected:
    //!< \cond DOXY_IMPL
    Modulation m_modulation = Modulation::NOT_SET;  //!< The modulation used to send the packet/data
    std::vector<uint8_t> m_data;                    //!< The packet/data send over the air
    sc_time m_sent_time;
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
