/*!
 * \file esyslora/sim/sx1272.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/radiobase.h"
#include "esyslora/radiocallback.h"
#include "esyslora/sim/radio.h"

namespace esyslora
{

namespace sim
{

/*! \class RadioBase esyslora/sim/sx1272.h "esyslora/sim/sx1272.h"
 *  \brief
 *
 */
class ESYSLORA_API SX1272 : public Radio, public RadioBase
{
public:
    SC_HAS_PROCESS(SX1272);

    enum class InternalState
    {
        IDLE,
        GOING_TO_RX,
        RX,
        GOING_TO_IDLE,
        GOING_TO_TX,
        TX,
        GOING_TX_TO_IDLE
    };

    //! Default constructor
    SX1272();

    //! Constructor
    /*!
     * \param[in] name the name of the SX1272
     */
    SX1272(const sc_module_name &name);

    //! Destructor
    virtual ~SX1272();


    virtual void set_modulation(Modulation modulation) override;
    virtual Modulation get_modulation() const override;

    virtual int set_lora_cfg(const LoRaCfg &lora_cfg) override;
    virtual const LoRaCfg &get_lora_cfg() const override;
    virtual LoRaCfg &get_lora_cfg() override;

    virtual void set_tx_power(int8_t tx_power) override;
    virtual int8_t get_tx_power() override;

    virtual Status get_status() const override;

    virtual void set_frequency(double frequency) override;
    virtual double get_frequency() const override;

    virtual bool is_channel_free(int16_t rssi_threshold, uint32_t max_carrier_sense_time) override;

    virtual int tx(uint8_t *packet, uint32_t size) override;

    virtual int rx(uint32_t timeout) override;

    virtual void start_channel_activity_detection() override;

    virtual int16_t get_rssi() override;

    virtual uint32_t get_wake_up_time() override;

    void set_internal_state(InternalState internal_state);
    InternalState get_internal_state();

    void set_rx_timed_out(bool rx_timed_out);
    bool get_rx_timed_out() const;

    //AirMsgCb_if
    virtual void rx_msg(std::shared_ptr<AirMsg> msg) override;
    virtual void tx_done() override;

    void main();

protected:
    void set_status(Status status);

    Status m_status = Status::IDLE;
    Modulation m_modulation = Modulation::NOT_SET;
    int8_t m_tx_power = 0;
    double m_frequency = 0.0;

    std::shared_ptr<AirMsg> m_rx_msg;
    std::shared_ptr<AirMsg> m_tx_msg;
    InternalState m_internal_state = InternalState::IDLE;
    bool m_rx_timed_out = false;

    sc_event m_next_internal_state_evt;

    sc_time m_rx_timeout;

    sc_time m_idle_to_rx{100.0, SC_NS};
    sc_time m_rx_to_idle{100.0, SC_NS};
    sc_time m_idle_to_tx{100.0, SC_NS};
    sc_time m_tx_to_idle{100.0, SC_NS};
};

} // namespace sim

} // namespace esyslora
