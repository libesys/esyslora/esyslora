/*!
 * \file esyslora/sim/channel.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg_if.h"
#include "esyslora/sim/channel_if.h"
#include "esyslora/sim/channelport.h"
#include "esyslora/sim/radio.h"
#include "esyslora/sim/msgqueue.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class Channel esyslora/sim/channel.h "esyslora/sim/channel.h"
 *  \brief
 *
 */
class ESYSLORA_API Channel : public sc_module, public Channel_if
{
public:
    SC_HAS_PROCESS(Channel);

    //! Constructor
    Channel(const sc_module_name &name);

    //! Destructor
    virtual ~Channel();

    //! Add a Radio to the channel
    /*!
     * \param[in] radio the Radio added to the channel
     */
    void add(Radio *radio);

    //! Get all channel ports
    /*!
     * \return all channel ports
     */
    std::vector<std::shared_ptr<ChannelPort>> &get_ports();

    //! Get all channel ports
    /*!
     * \return all channel ports
     */
    const std::vector<std::shared_ptr<ChannelPort>> &get_ports() const;

    // Channel_if
    virtual void ch_tx_msg(std::shared_ptr<ChannelMsg> msg) override;

    //! The main thread of the channel
    void main();

protected:
    //!< \cond DOXY_IMPL
    std::vector<std::shared_ptr<ChannelPort>> m_ports;              //!< All the channel ports
    esyslora::sim::MsgQueue<esyslora::sim::ChannelMsg> m_msg_queue; //!< The queue of messages currently on the air
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
