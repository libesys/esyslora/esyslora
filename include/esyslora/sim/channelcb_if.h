/*!
 * \file esyslora/sim/channelcb_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/channelmsg.h"

#include <systemc.h>

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class ChannelCb_if esyslora/sim/channelcb_if.h "esyslora/sim/channelcb_if.h"
 *  \brief
 *
 */
class ESYSLORA_API ChannelCb_if
{
public:
    //! Constructor
    ChannelCb_if();

    //! Destructor
    virtual ~ChannelCb_if();

    //! Indicates that the reception of a message has started
    /*!
     * \param[in] msg the message being currently being received
     */
    virtual void chcb_msg_start(std::shared_ptr<ChannelMsg> msg) = 0;

    //! Indicates that the reception of a message has completed
    /*!
     * \param[in] msg the received message
     */
    virtual void chcb_msg_done(std::shared_ptr<ChannelMsg> msg) = 0;
};

} // namespace sim

} // namespace esyslora
