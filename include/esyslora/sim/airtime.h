/*!
 * \file esyslora/sim/airtime.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/airmsg.h"

namespace esyslora
{

namespace sim
{

/*! \class AirTime esyslora/sim/airtime.h "esyslora/sim/airtime.h"
 *  \brief Class used to calculate the air time of a LoRa packet
 *
 */
class ESYSLORA_API AirTime
{
public:
    //! Default Constructor
    AirTime();

    //! Constructor
    AirTime(AirMsg *air_msg);

    //! Constructor
    AirTime(LoRaCfg *lora_cfg);

    //! Destructor
    virtual ~AirTime();

    //! Set LoRa packet 
    /*!
     * \param[in] air_msg the LoRa packet
     */
    void set_air_msg(AirMsg *air_msg);

    //! Get LoRa packet
    /*!
     * \return the LoRa packet
     */
    AirMsg *get_air_msg() const;

    //! Set LoRa configuration
    /*!
     * \param[in] lora_cfg the LoRa configuration
     */
    void set_lora_cfg(LoRaCfg *lora_cfg);

    //! Get LoRa configuration
    /*!
     * \return the LoRa configuration
     */
    LoRaCfg *get_lora_cfg() const;

    //! Get the bandwidth in Hz
    /*!
     * \return the bandwidth in Hz
     */
    double get_bandwith();

    //! Get the spreading factor
    /*!
     * \return the spreading factor 6 to 12
     */
    std::size_t get_spreading_factor();

    //! Get the symbol rate in Hz
    /*!
     * \return the symbol rate
     */
    double get_symbol_rate();

    //! Get the symbol period in sec
    /*!
     * \return the symbol period in sec
     */
    double get_symbol_period();

    //! Get the premble time in sec
    /*!
     * \return the preamble in sec
     */
    double get_preamble_time();

    //! Get the preamble size in symbols
    /*!
     * \return the preamble size in symbols
     */
    double get_preamble_size();

    //! Get the error coding rate
    /*!
     * \return the error coding rate
     */
    std::size_t get_coding_rate();

    //! Get the payload size in symbols of the stored AirMsg
    /*!
     * \return the payload size in symbols
     */
    std::size_t get_payload_size();

    //! Get the payload size in symbols for the given payload size
    /*!
     * \return the payload size in symbols
     */
    std::size_t get_payload_size(int payload_size);

    //! Get the time on air of the payload in sec of the stored AirMsg
    /*!
     * \return the time on air of the payload in sec
     */
    double get_payload_time();

    //! Get the time on air of the payload size
    /*!
     * \return the time on air of the payload in sec
     */
    double get_payload_time(int payload_size);

    //! Get the time on air of the packet stored in sec
    /*!
     * \return the time on air of the packet in sec
     */
    double get_msg_time();

    //! Get the time on air of the packet in sec with a given payload size
    /*!
     * \return the time on air of the packet in sec
     */
    double get_msg_time(int payload_size);

    //! Get the size of the stored packet in symbols
    /*!
     * \return the size of the packet in symbols
     */
    double get_msg_size();

    //! Get the size of the packet in symbols with a given payloadr size
    /*!
     * \return the size of the packet in symbols
     */
    double get_msg_size(int payload_size);


    int max_payload_under_time(double air_time, int max_payload = 256);

protected:
    //!< \cond DOXY_IMPL
    AirMsg *m_air_msg = nullptr;
    LoRaCfg *m_lora_cfg = nullptr;
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
