/*!
 * \file esyslora/sim/channel_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/sim/channelmsg.h"
#include "esyslora/sim/channelcb_if.h"

#include <memory>

namespace esyslora
{

namespace sim
{

/*! \class Channel_if esyslora/sim/channel_if.h "esyslora/sim/channel_if.h"
 *  \brief
 *
 */
class ESYSLORA_API Channel_if
{
public:
    //! Constructor
    Channel_if();

    //! Desstructor
    virtual ~Channel_if();

    //! A ChannelMsg is transmitted
    /*!
     * \param[in] msg the message
     */
    virtual void ch_tx_msg(std::shared_ptr<ChannelMsg> msg) = 0;

    //! Set the callback interface 
    /*!
     * \param[in] cb_if the callback interface
     */
    void set_cb_if(ChannelCb_if *cb_if);

    //! Get the callback interface
    /*!
     * \return the callback interface
     */
    ChannelCb_if *get_cb_if();

protected:
    //!< \cond DOXY_IMPL
    ChannelCb_if *m_cb_if = nullptr;    //!< The callback interface
    //!< \endcond
};

} // namespace sim

} // namespace esyslora
#pragma once
