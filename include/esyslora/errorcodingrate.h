/*!
 * \file esyslora/errorcodingrate.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"

#include <ostream>

namespace esyslora
{

enum class ErrorCodingRate
{
    EC4_5,
    EC4_6,
    EC4_7,
    EC4_8,
    NOT_SET = -1
};

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::ErrorCodingRate error_coding_rate);