/*!
 * \file esyslora/spreadingfactor.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"

#include <ostream>

namespace esyslora
{

enum class SpreadingFactor
{
    SF6,
    SF7,
    SF8,
    SF9,
    SF10,
    SF11,
    SF12,
    NOT_SET = -1
};

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::SpreadingFactor spreading_factor);
