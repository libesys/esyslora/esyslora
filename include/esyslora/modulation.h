/*!
 * \file esyslora/modulation.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"

#include <ostream>

namespace esyslora
{

enum class Modulation
{
    FSK_OOK,
    LORA,
    NOT_SET = -1
};

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::Modulation modulation);
