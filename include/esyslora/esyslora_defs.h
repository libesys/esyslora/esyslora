/*!
 * \file esyslora/esyslora_defs.h
 * \brief Definitions needed for esyslora
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSLORA_EXPORTS
#define ESYSLORA_API __declspec(dllexport)
#elif ESYSLORA_USE
#define ESYSLORA_API __declspec(dllimport)
#else
#define ESYSLORA_API
#endif

#ifndef ESYSLORA_EXPORTS
#include "esyslora/config.h"
#endif
