/*!
 * \file esyslora/bandwidth.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"

#include <ostream>

namespace esyslora
{

enum class Bandwidth
{
    BW125KHZ,
    BW250KHZ,
    BW500KHZ,
    NOT_SET = -1
};

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::Bandwidth bandwidth);
