/*!
 * \file esyslora/version.h
 * \brief Version info for esyslora
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSLORA_MAJOR_VERSION 0
#define ESYSLORA_MINOR_VERSION 0
#define ESYSLORA_RELEASE_NUMBER 1
#define ESYSLORA_VERSION_STRING "ESysLORA 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSLORA_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSLORA_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSLORA_VERSION_NUMBER \
    (ESYSLORA_MAJOR_VERSION * 1000) + (ESYSLORA_MINOR_VERSION * 100) + ESYSLORA_RELEASE_NUMBER
#define ESYSLORA_BETA_NUMBER 1
#define ESYSLORA_VERSION_FLOAT                                                                   \
    ESYSLORA_MAJOR_VERSION + (ESYSLORA_MINOR_VERSION / 10.0) + (ESYSLORA_RELEASE_NUMBER / 100.0) \
        + (ESYSLORA_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSLORA_CHECK_VERSION(major, minor, release)                                                            \
    (ESYSLORA_MAJOR_VERSION > (major) || (ESYSLORA_MAJOR_VERSION == (major) && ESYSLORA_MINOR_VERSION > (minor)) \
     || (ESYSLORA_MAJOR_VERSION == (major) && ESYSLORA_MINOR_VERSION == (minor)                                  \
         && ESYSLORA_RELEASE_NUMBER >= (release)))
