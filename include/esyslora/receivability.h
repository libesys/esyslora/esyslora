/*!
 * \file esyslora/receivability.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esyslora/esyslora_defs.h"
#include "esyslora/loracfg.h"

namespace esyslora
{

/*! \class LoRaCfg esyslora/receivability.h "esyslora/receivability.h"
 *  \brief
 *
 */
class ESYSLORA_API Receivability
{
public:
    //! Constructor
    Receivability();

    //! Tells if a message can be received by a radio with a given LoRa configuration
    /*!
     * \return true if the message can be received, false otherwise
     */
    bool operator()(const LoRaCfg &receiver_cfg, const LoRaCfg &recv_msg_cfg);

    //! Set the range of frequencies in Hz a radio can receive based on its fundamental frequency
    /*!
     * \param[in] frequency_variation the width of the range of frequencies centered on the fundamental frequency
     */
    void set_frequency_variation(double frequency_variation);

    //! Get the range of frequencies in Hz a radio can receive based on its fundamental frequency
    /*!
     * \return the width in Hz of the range of frequencies centered on the fundamental frequency
     */
    double get_frequency_variation();
protected:
    //!< \cond DOXY_IMPL
    double m_frequency_variation = 1000.0; //!< 1 KHz default
    //!< \endcond
};

} // namespace esyslora
