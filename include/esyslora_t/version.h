/*!
 * \file esyslora/version.h
 * \brief Version info for esyslora_t
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYSLORA_T_VERSION_H__
#define __ESYSLORA_T_VERSION_H__

// Bump-up with each new version
#define ESYSLORA_T_MAJOR_VERSION    0
#define ESYSLORA_T_MINOR_VERSION    0
#define ESYSLORA_T_RELEASE_NUMBER   1
#define ESYSLORA_T_VERSION_STRING   _T("esyslora_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define ESYSLORA_T_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYSLORA_T_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYSLORA_T_VERSION_NUMBER (ESYSLORA_T_MAJOR_VERSION * 1000) + (ESYSLORA_T_MINOR_VERSION * 100) + ESYSLORA_T_RELEASE_NUMBER
#define ESYSLORA_T_BETA_NUMBER      1
#define ESYSLORA_T_VERSION_FLOAT ESYSLORA_T_MAJOR_VERSION + (ESYSLORA_T_MINOR_VERSION/10.0) + (ESYSLORA_T_RELEASE_NUMBER/100.0) + (ESYSLORA_T_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYSLORA_T_CHECK_VERSION(major,minor,release) \
    (ESYSLORA_T_MAJOR_VERSION > (major) || \
    (ESYSLORA_T_MAJOR_VERSION == (major) && ESYSLORA_T_MINOR_VERSION > (minor)) || \
    (ESYSLORA_T_MAJOR_VERSION == (major) && ESYSLORA_T_MINOR_VERSION == (minor) && ESYSLORA_T_RELEASE_NUMBER >= (release)))

#endif

