/*!
 * \file esyslora_t/sim/msgqueue01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"

#include <esyslora/sim/msgqueue.h>
#include <esyslora/sim/channelmsg.h>

#include <sysc/sc_simulation.h>

namespace esyslora_t
{

namespace sim
{

namespace msgqueue01
{

class simulation : public sc_simulation
{
public:
    simulation();

    virtual int main() override;

protected:
};

simulation::simulation()
{
}

int simulation::main()
{
    sc_start(10000, SC_MS);

    return 0;
}

class Test : public sc_module
{
public:
    SC_HAS_PROCESS(Test);

    Test(const sc_module_name &name);
    virtual ~Test();

    void main();

protected:
    esyslora::sim::MsgQueue<esyslora::sim::ChannelMsg> m_msg_queue;
};

Test::Test(const sc_module_name &name)
    : sc_module(name)
{
    SC_THREAD(main);
}

Test::~Test()
{
}

void Test::main()
{
    std::shared_ptr<esyslora::sim::ChannelMsg> msg0 = std::make_shared<esyslora::sim::ChannelMsg>();
    std::shared_ptr<esyslora::sim::ChannelMsg> msg1 = std::make_shared<esyslora::sim::ChannelMsg>();

    msg0->set_start_time(sc_time_stamp());
    msg0->set_end_time(sc_time_stamp() + sc_time(100.0, SC_MS));
    m_msg_queue.add(msg0);

    ESYSTEST_REQUIRE_EQUAL(m_msg_queue.size(), 1);

    wait(10.0, SC_MS);

    msg1->set_start_time(sc_time_stamp());
    msg1->set_end_time(sc_time_stamp() + sc_time(20.0, SC_MS));

    m_msg_queue.add(msg1);

    ESYSTEST_REQUIRE_EQUAL(m_msg_queue.size(), 2);
    ESYSTEST_REQUIRE_EQUAL(m_msg_queue[0].m_msg, msg1);

    wait(m_msg_queue.get_event());

    sc_time evt_time = sc_time_stamp();
    ESYSTEST_REQUIRE_EQUAL(evt_time, sc_time(30.0, SC_MS));

    m_msg_queue.pop(true);

    wait(m_msg_queue.get_event());

    evt_time = sc_time_stamp();
    ESYSTEST_REQUIRE_EQUAL(evt_time, sc_time(100.0, SC_MS));

    m_msg_queue.pop(true);
    ESYSTEST_REQUIRE_EQUAL(m_msg_queue.size(), 0);
}

/*! \class MsgQueue01 esyslora_t/sim/msgqueue01.cpp "esyslora_t/sim/msgqueue01.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(MsgQueue01)
{
    simulation simul;

    Test test("Test");

    sc_simulation::init();

    int result = simul.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace msgqueue01

} // namespace sim

} // namespace esyslora_t
