/*!
 * \file esyslora_t/sim/airtime03_sim.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"
#include "esyslora_t/testcasectrl.h"

#include <esyslora/sim/airtime.h>

#include <boost/filesystem.hpp>

#include <cmath>
#include <iostream>
#include <vector>

namespace esyslora_t
{

namespace sim
{

class Data
{
public:
    class SizeTime
    {
    public:
        SizeTime(unsigned int size, double time)
            : m_size(size)
            , m_time(time)
        {
        }

        unsigned int get_size()
        {
            return m_size;
        }

        double get_time()
        {
            return m_time;
        }

    protected:
        unsigned int m_size;
        double m_time;
    };

    Data(const esyslora::LoRaCfg &lora_cfg, const std::vector<SizeTime> &size_times);

    esyslora::LoRaCfg &get_lora_cfg();
    std::vector<SizeTime> &get_size_times();

protected:
    esyslora::LoRaCfg m_lora_cfg;
    std::vector<SizeTime> m_size_times;
};

Data::Data(const esyslora::LoRaCfg &lora_cfg, const std::vector<SizeTime> &size_times)
    : m_lora_cfg(lora_cfg)
    , m_size_times(size_times)
{
}

esyslora::LoRaCfg &Data::get_lora_cfg()
{
    return m_lora_cfg;
}

std::vector<Data::SizeTime> &Data::get_size_times()
{
    return m_size_times;
}

void test_time(double airtime_time, double time_ref, double delta_time)
{
    ESYSTEST_REQUIRE_LE(airtime_time, time_ref + delta_time);
    ESYSTEST_REQUIRE_GE(time_ref + delta_time, airtime_time);
}

/*! \class AirTime03 esyslora_t/sim/airtime03_sim.cpp "esyslora_t/sim/airtime03_sim.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(AirTime03)
{
    std::string env_var;

    Data data0({esyslora::Bandwidth::BW125KHZ, esyslora::ErrorCodingRate::EC4_6, esyslora::SpreadingFactor::SF8},
               {{10, 78.34}, {50, 201.22}, {100, 360.96}});

    Data data1({esyslora::Bandwidth::BW125KHZ, esyslora::ErrorCodingRate::EC4_5, esyslora::SpreadingFactor::SF8},
               {{10, 72.19}, {50, 174.59}, {100, 307.71}});

    Data data2({esyslora::Bandwidth::BW125KHZ, esyslora::ErrorCodingRate::EC4_6, esyslora::SpreadingFactor::SF7},
               {{10, 45.31}, {50, 112.9}, {100, 205.06}});

    Data data3({esyslora::Bandwidth::BW125KHZ, esyslora::ErrorCodingRate::EC4_5, esyslora::SpreadingFactor::SF7},
               {{10, 41.22}, {50, 97.54}, {100, 174.34}});

    Data data4({esyslora::Bandwidth::BW250KHZ, esyslora::ErrorCodingRate::EC4_6, esyslora::SpreadingFactor::SF8},
               {{10, 39.17}, {50, 100.61}, {100, 180.48}});


    std::vector<Data *> datas = {&data0, &data1, &data2, &data3, &data4};

    auto &mts = esystest::MasterTestSuite::Get();
    auto &ctrl = TestCaseCtrl::Get();

    
    double airtime_time;
    double delta_time = 0.005;

    for (auto data : datas)
    {
        esyslora::sim::AirTime airtime;
        esyslora::sim::AirTime airtime1;
        esyslora::sim::AirMsg airmsg;

        airtime.set_lora_cfg(&data->get_lora_cfg());
        airmsg.set_lora_cfg(data->get_lora_cfg());
        airtime1.set_air_msg(&airmsg);

        for (auto &size_time : data->get_size_times())
        {
            airtime_time = airtime.get_msg_time(size_time.get_size()) * 1000.0;
            test_time(airtime_time, size_time.get_time(), delta_time);
            
            airmsg.set_size(size_time.get_size());            
            airtime_time = airtime1.get_msg_time() * 1000.0;
            test_time(airtime_time, size_time.get_time(), delta_time);
        }
    }
}

} // namespace sim

} // namespace esyslora_t
