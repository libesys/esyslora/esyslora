/*!
 * \file esyslora_t/sim/channel01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"

#include <esyslora/sim/channel.h>
#include <esyslora/sim/sx1272.h>

#include <sysc/sc_simulation.h>
#include <systemc.h>

#include <map>

namespace esyslora_t
{

namespace sim
{

namespace channel01
{

class simulation : public sc_simulation
{
public:
    simulation();

    virtual int main() override;

protected:
};

simulation::simulation()
{
}

int simulation::main()
{
    sc_start(10000, SC_MS);

    return 0;
}

/*! \class Test esyslora_t/sim/channel01.cpp "esyslora_t/sim/channel01.cpp"
 *  \brief
 *
 */
class Test : public sc_module, public esyslora::RadioCallback
{
public:
    SC_HAS_PROCESS(Test);

    const static int RADIO_COUNT = 3;

    //! Constructor
    Test(const sc_module_name &name);

    //! Destructor
    virtual ~Test();

    // esyslora::RadioCallback
    virtual void event(esyslora::RadioBase *radio, Event evt) override;
    virtual void rx_done(esyslora::RadioBase *radio, uint8_t *data, uint32_t size, int16_t rssi, int8_t snr) override;
    virtual void freq_hoping_change(esyslora::RadioBase *radio, uint8_t current_channel) override;
    virtual void channel_activity_detection_done(esyslora::RadioBase *radio, bool activity_detected) override;

    // Main thread
    void main();

    //! Add a received event from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which sent the event 
     * \param[in] evt the Event
     */
    void add_event(int radio_id, esyslora::RadioCallback::Event evt);

    //! Get the number of a given Event received from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio of interest 
     * \param[in] evt the Event
     * \return the number of times this Event was received from the given radio
     */
    int get_event_count(int radio_id, esyslora::RadioCallback::Event evt) const;

    //! Add a received message from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which received the message 
     * \param[in] data the buffer
     * holding the message
     * \paran[in] size the size of the message in bytes
     */
    void add_receive_msg(int radio_id, uint8_t *data, uint32_t size);

    //! Get all received message from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which received the message
     * \return all the received messages
     */
    std::vector<std::vector<uint8_t>> &get_rx_msg(int radio_id);

protected:
    //!< \cond DOXY_IMPL
    esyslora::sim::Channel m_channel{"AirChannel"};                                 //!< The Channel modeling LoRa
    esyslora::sim::SX1272 m_radio0{"Radio0"};                                       //!< The Radio 0
    esyslora::sim::SX1272 m_radio1{"Radio1"};                                       //!< The Radio 1
    esyslora::sim::SX1272 m_radio2{"Radio2"};                                       //!< The Radio 2
    esyslora::LoRaCfg m_lora_cfg;                                                   //!< LoRa configuration to use
    std::vector<esyslora::sim::SX1272 *> m_radios{&m_radio0, &m_radio1, &m_radio2}; //!< Vector of all radios
    std::map<esyslora::RadioCallback::Event, int> m_map_events[RADIO_COUNT];       //!< Store events per Radio
    std::vector<std::vector<uint8_t>> m_rx_msg[RADIO_COUNT]; //!< Store receiced messages per Radio
    //!< \endcond
};

Test::Test(const sc_module_name &name)
    : sc_module(name)
{
    SC_THREAD(main);

    m_lora_cfg.set_bandwidth(esyslora::Bandwidth::BW250KHZ);
    m_lora_cfg.set_error_coding_rate(esyslora::ErrorCodingRate::EC4_6);
    m_lora_cfg.set_frequency(868.0E+06);
    m_lora_cfg.set_header_mode(esyslora::HeaderMode::IMPLICIT);
    m_lora_cfg.set_preamble_size(8);
    m_lora_cfg.set_spreading_factor(esyslora::SpreadingFactor::SF8);
    m_lora_cfg.set_use_crc(false);
    m_lora_cfg.set_use_low_data_rate_optimization(false);

    for (int idx = 0; idx < RADIO_COUNT; ++idx)
    {
        m_radios[idx]->set_id(idx);
        m_radios[idx]->set_callback(this);
        m_radios[idx]->set_lora_cfg(m_lora_cfg);
        m_channel.add(m_radios[idx]);
    }
}

Test::~Test()
{
}

void Test::event(esyslora::RadioBase *radio, Event evt)
{
    add_event(radio->get_id(), evt);
}

void Test::rx_done(esyslora::RadioBase *radio, uint8_t *data, uint32_t size, int16_t rssi, int8_t snr)
{
    add_receive_msg(radio->get_id(), data, size);
}

void Test::freq_hoping_change(esyslora::RadioBase *radio, uint8_t current_channel)
{
}

void Test::channel_activity_detection_done(esyslora::RadioBase *radio, bool activity_detected)
{
}

void Test::add_event(int radio_id, esyslora::RadioCallback::Event evt)
{
    std::map<esyslora::RadioCallback::Event, int>::iterator it;

    it = m_map_events[radio_id].find(evt);
    if (it == m_map_events[radio_id].end())
    {
        m_map_events[radio_id][evt] = 1;
        return;
    }
    it->second++;
}

int Test::get_event_count(int radio_id, esyslora::RadioCallback::Event evt) const
{
    std::map<esyslora::RadioCallback::Event, int>::const_iterator it;

    it = m_map_events[radio_id].find(evt);
    if (it == m_map_events[radio_id].end()) return 0;
    return it->second;
}

void Test::add_receive_msg(int radio_id, uint8_t *data, uint32_t size)
{
    std::vector<uint8_t> msg;

    msg.resize(size);
    memcpy(msg.data(), data, size);

    m_rx_msg[radio_id].push_back(msg);
}

std::vector<std::vector<uint8_t>> &Test::get_rx_msg(int radio_id)
{
    return m_rx_msg[radio_id];
}

void Test::main()
{
    std::vector<uint8_t> data0 = {0x01, 0x02, 0x03, 0x04};
    std::vector<uint8_t> data1 = {0x05, 0x06, 0x07, 0x08};

    // Step 1: test reception of event RX_TIMEOUT, when no LoRa message was received
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, esyslora::RadioCallback::Event::RX_TIMEOUT), 0);

    m_radio0.rx(1000);

    wait(1010.0, SC_MS);

    // Check the that Radio 0 did timeout
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, esyslora::RadioCallback::Event::RX_TIMEOUT), 1);
    // And that Radio 0 didn't receive any message
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0).size(), 0);

    // Step 2: test reception of a LoRa message
    m_radio0.rx(1000);

    wait(10.0, SC_MS);

    // Make sure that the TX_DONE counter for Radio 1 is indeed 0
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, esyslora::RadioCallback::Event::TX_DONE), 0);

    m_radio1.tx(data0.data(), (uint32_t)data0.size());

    wait(990.0, SC_MS); // Wait long enough to make sure the message was fully transmitted

    // Test that the Radio1 did receive the event TX_DONE
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, esyslora::RadioCallback::Event::TX_DONE), 1);

    // Test if the receive message is identical to the message sent
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0).size(), 1);
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0)[0].size(), data0.size());
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0)[0], data0);

    // Step 3: test the collision of 2 messages
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, esyslora::RadioCallback::Event::RX_TIMEOUT), 1);

    // Radio 0 is set to receive a message
    m_radio0.rx(1000);

    wait(10.0, SC_MS);

    // The Radio 1 send a message
    m_radio1.tx(data0.data(), (uint32_t)data0.size());

    wait(1.0, SC_MS);

    // After a small delay, Radio 2 send a message with the exact same LoRa configuration
    m_radio2.tx(data1.data(), (uint32_t)data1.size());

    // We wait long enough to have the Radio 0 timing out of the message reception.
    wait(1010.0, SC_MS);

    // Check that Radio 1 has indeed sent 2 messages so far
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, esyslora::RadioCallback::Event::TX_DONE), 2);
    // Check that Radio 2 has just sent 1 message so far
    ESYSTEST_REQUIRE_EQUAL(get_event_count(2, esyslora::RadioCallback::Event::TX_DONE), 1);
    // Check that since there was an interference between messages sent by Radio 1 and 2, Radio 0 did timeout once more
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, esyslora::RadioCallback::Event::RX_TIMEOUT), 2);
}

/*! \class Channel01 esyslora_t/sim/channel01.cpp "esyslora_t/sim/channel01.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(Channel01)
{
    simulation simul;

    Test test("Test");

    sc_simulation::init();

    int result = simul.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace channel01

} // namespace sim

} // namespace esyslora_t
