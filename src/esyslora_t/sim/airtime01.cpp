/*!
 * \file esyslora_t/sim/airtime01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"

#include <esyslora/sim/airtime.h>

#include <matplotlibcpp.h>

#include <boost/locale.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <stdlib.h>

#include <cmath>
#include <iostream>

namespace plt = matplotlibcpp;

namespace esyslora_t
{

namespace sim
{

class MyInterpreter : public matplotlibcpp::Interpreter
{
public:
    MyInterpreter();
    virtual ~MyInterpreter();

protected:
};

MyInterpreter::MyInterpreter()
    : matplotlibcpp::Interpreter()
{
}

MyInterpreter::~MyInterpreter()
{
}

/*! \class AirTime01 esyslora_t/sim/airtime01.cpp "esyslora_t/sim/airtime01.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(AirTime01)
{
    std::shared_ptr<MyInterpreter> interpreter = std::make_shared<MyInterpreter>();
    std::string env_var;

#ifdef _M_AMD64
    env_var = "ESYS_PYTHONEXE64";
#else
    env_var = "ESYS_PYTHONEXE32";
#endif
    char *env_var_value = std::getenv(env_var.c_str());

    auto &mts = esystest::MasterTestSuite::Get();

    matplotlibcpp::Interpreter::set(interpreter);

    std::vector<std::string> folders = {"DLLs", "Lib", "Lib\\site-packages"}; // "lib"};

    if (env_var_value != nullptr)
    {
        interpreter->set_root_path(env_var_value);

        for (auto &folder : folders) interpreter->add_sys_path_rel_root(folder);
    }
   
    esyslora::sim::AirTime airtime;
    esyslora::sim::AirMsg airmsg;

    airtime.set_air_msg(&airmsg);

    airmsg.get_lora_cfg().set_bandwidth(esyslora::Bandwidth::BW250KHZ);
    airmsg.get_lora_cfg().set_error_coding_rate(esyslora::ErrorCodingRate::EC4_6);
    airmsg.get_lora_cfg().set_frequency(868.0E+06);
    airmsg.get_lora_cfg().set_header_mode(esyslora::HeaderMode::IMPLICIT);
    airmsg.get_lora_cfg().set_preamble_size(8);
    airmsg.get_lora_cfg().set_spreading_factor(esyslora::SpreadingFactor::SF8);
    airmsg.get_lora_cfg().set_use_crc(false);
    airmsg.get_lora_cfg().set_use_low_data_rate_optimization(false);

    std::vector<std::size_t> byte_counts;
    std::vector<std::size_t> bit_counts;
    std::vector<double> sizes;

    for (std::size_t byte_count = 1; byte_count < 100; ++byte_count)
    {
        byte_counts.push_back(byte_count);
        bit_counts.push_back(byte_count);

        airmsg.set_size(byte_count);

        sizes.push_back((double)airtime.get_payload_size());
    }

    airmsg.set_size(51);

    std::cout << airmsg << std::endl;
    std::cout << "Timing info:" << std::endl;
    std::cout << "    Premable TOA  : " << airtime.get_preamble_time() * 1000.0 << " ms" << std::endl;
    std::cout << "    Symbol TOA    : " << airtime.get_symbol_period() * 1000.0 << " ms" << std::endl;
    std::cout << "    Symbols count : " << airtime.get_payload_size() << " + " << airtime.get_preamble_size()
              << std::endl;
    std::cout << "    TOA           : " << airtime.get_msg_time() * 1000.0 << " ms" << std::endl;

#if defined(_MSC_VER) && !defined(_DEBUG)
    // Set the size of output image to 1200x780 pixels
    plt::figure_size(1200, 780);
    // Plot line from given x and y data. Color is selected automatically.
    plt::plot(byte_counts, sizes);
    // Plot a red dashed line from given x and y data.
    plt::xlim(0, 100);
    // plt::ylim(0, 140);

    plt::xlabel("Payload (bytes)");
    plt::ylabel("Payload (symbols)");
    // Add graph title
    plt::title("LoRa payload vs tx symbols");
    // Enable legend.
    plt::legend();
    // Save the image (file format is determined by the extension)
    plt::grid(true);
    plt::show();
    // plt::save("./basic.png");
#endif
}

} // namespace sim

} // namespace esyslora_t
