/*!
 * \file esyslora_t/sim/airtime02_sim.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"
#include "esyslora_t/testcasectrl.h"

#include <esyslora/sim/airtime.h>

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

#include <xlnt/xlnt.hpp>

#include <boost/filesystem.hpp>

#include <cmath>
#include <iostream>

namespace esyslora_t
{

namespace sim
{

/*! \class AirTime02 esyslora_t/sim/airtime02_sim.cpp "esyslora_t/sim/airtime02_sim.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(AirTime02)
{
    std::string env_var;

    auto &mts = esystest::MasterTestSuite::Get();
    auto &ctrl = TestCaseCtrl::Get();

    esyslora::sim::AirTime airtime;
    esyslora::LoRaCfg lora_cfg;

    airtime.set_lora_cfg(&lora_cfg);

    lora_cfg.set_bandwidth(esyslora::Bandwidth::BW125KHZ);
    lora_cfg.set_error_coding_rate(esyslora::ErrorCodingRate::EC4_5);
    lora_cfg.set_frequency(868.0E+06);
    lora_cfg.set_header_mode(esyslora::HeaderMode::EXPLICIT);
    lora_cfg.set_preamble_size(8);
    lora_cfg.set_spreading_factor(esyslora::SpreadingFactor::SF8);
    lora_cfg.set_use_crc(true);
    lora_cfg.set_use_low_data_rate_optimization(false);

    boost::filesystem::path temp_folder = ctrl.GetTempFilesFolder();
    temp_folder /= "example.xlsx";

    xlnt::workbook wb;
    xlnt::worksheet ws = wb.active_sheet();

    ws.cell("A1").value("Air time for a given payload");
    ws.merge_cells("A1:D1");
    ws.cell("A3").value("payload (bytes)");
    ws.cell("B3").value("air time (ms)");

	int toa_start_row = 4;
    std::cout << "Payload size ->  TOA:" << std::endl;
    for (int size = 0; size < 257; ++size)
    {
        auto toa = airtime.get_msg_time(size) * 1000.0;

        ws.cell({"A", (uint32_t)(toa_start_row + size)}).value(size);
        ws.cell({"B", (uint32_t)(toa_start_row + size)}).value(toa);
        std::cout << size << " -> " << toa << " ms" << std::endl;
    }

    wb.save(temp_folder.c_str());

    std::cout << std::endl;

    std::cout << "Max payload size in bytes under a given OTA time:" << std::endl;
    std::vector<double> times = {418 / 1024.0, 2732 / 1024.0};

    for (auto time : times)
    {
        auto size = airtime.max_payload_under_time(time, 10000);

        std::cout << time << "s " << airtime.max_payload_under_time(time, 10000);
        if (size < 0)
            std::cout << std::endl;
        else
            std::cout << " -> " << airtime.get_msg_time(size) * 1000.0 << " ms" << std::endl;
    }
}

} // namespace sim

} // namespace esyslora_t
