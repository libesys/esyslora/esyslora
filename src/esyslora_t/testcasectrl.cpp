/*!
 * \file esyslora_t/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora_t/esyslora_t_prec.h"
#include "esyslora_t/testcasectrl.h"

#include <cassert>

namespace esyslora_t
{

TestCaseCtrl *TestCaseCtrl::g_test_case = nullptr;

TestCaseCtrl &TestCaseCtrl::Get()
{
    assert(g_test_case != nullptr);

    return *g_test_case;
}

TestCaseCtrl::TestCaseCtrl()
    : esystest::TestCaseCtrl()
{
    g_test_case = this;

    AddSearchPathEnvVar("ESYSLORA");
    AddSearchPath("res/esyslora_t");                    // If cwd is root of the emdev git repo
    AddSearchPath("../../src/esyslora/res/esyslora_t"); // if cwd is the bin folder
}

TestCaseCtrl::~TestCaseCtrl()
{
}

} // namespace esyslora_t
