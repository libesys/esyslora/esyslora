/*!
 * \file esyslora/spreadingfactor.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/spreadingfactor.h"

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::SpreadingFactor spreading_factor)
{
    switch (spreading_factor)
    {
        case esyslora::SpreadingFactor::SF6: os << "6"; break;
        case esyslora::SpreadingFactor::SF7: os << "7"; break;
        case esyslora::SpreadingFactor::SF8: os << "8"; break;
        case esyslora::SpreadingFactor::SF9: os << "9"; break;
        case esyslora::SpreadingFactor::SF10: os << "10"; break;
        case esyslora::SpreadingFactor::SF11: os << "11"; break;
        case esyslora::SpreadingFactor::SF12: os << "12"; break;
        case esyslora::SpreadingFactor::NOT_SET:
        default: os << "NOT SET";
    };
    return os;
}
