/*!
 * \file esyslora/radiobase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/radiobase.h"

namespace esyslora
{

RadioBase::RadioBase()
{
}

RadioBase::~RadioBase()
{
}

void RadioBase::set_callback(RadioCallback *callback)
{
    m_callback = callback;
}

RadioCallback *RadioBase::get_callback() const
{
    return m_callback;
}

void RadioBase::set_id(int id)
{
    m_id = id;
}

int RadioBase::get_id() const
{
    return m_id;
}

void RadioBase::set_channel(uint32_t frequency)
{
    //! \TODO
}

void RadioBase::set_tx_config(Modulation modem, int8_t power, uint32_t fdev, uint32_t bandwidth, uint32_t datarate,
                              uint8_t coderate, uint16_t preambleLen, bool fixLen, bool crcOn, bool freqHopOn,
                              uint8_t hopPeriod, bool iqInverted, uint32_t timeout)
{
    //! \TODO
}

void RadioBase::set_rx_config(Modulation modem, uint32_t bandwidth, uint32_t datarate, uint8_t coderate,
                              uint32_t bandwidthAfc, uint16_t preambleLen, uint16_t symbTimeout, bool fixLen,
                              uint8_t payloadLen, bool crcOn, bool freqHopOn, uint8_t hopPeriod, bool iqInverted,
                              bool rxContinuous)
{
    //! \TODO
}

void RadioBase::set_max_payload_length(Modulation modem, uint8_t max)
{
    //! \TODO
}

void RadioBase::set_tx_continuous_wave(uint32_t freq, int8_t power, uint16_t time)
{
    //! \TODO
}

uint32_t RadioBase::time_on_air(Modulation modem, uint32_t bandwidth, uint32_t datarate, uint8_t coderate,
                                uint16_t preambleLen, bool fixLen, uint8_t payloadLen, bool crcOn)
{
    //! \TODO
    return 0;
}

bool RadioBase::check_rf_frequency(uint32_t frequency)
{
    //! \TODO
    return false;
}

void RadioBase::sleep()
{
    //! \TODO
}

void RadioBase::standby()
{
    //! \TODO
}

void RadioBase::set_public_network(bool enable)
{
    //! \TODO
}

uint32_t RadioBase::random()
{
    //! \TODO
    return 1;
}

} // namespace esyslora
