/*!
 * \file esyslora/sim/channel_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/channel_if.h"

namespace esyslora
{

namespace sim
{

Channel_if::Channel_if()
{
}

Channel_if::~Channel_if()
{
}

void Channel_if::set_cb_if(ChannelCb_if *rx_if)
{
    m_cb_if = rx_if;
}

ChannelCb_if *Channel_if::get_cb_if()
{
    return m_cb_if;
}

} // namespace sim

} // namespace esyslora
