/*!
 * \file esyslora/sim/airtime.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/airtime.h"

#include <cmath>
#include <cassert>

namespace esyslora
{

namespace sim
{

AirTime::AirTime()
{
}

AirTime::AirTime(AirMsg *air_msg)
    : m_air_msg(air_msg)
{
    if (air_msg != nullptr) m_lora_cfg = &air_msg->get_lora_cfg();
}

AirTime::AirTime(LoRaCfg *lora_cfg)
    : m_lora_cfg(lora_cfg)
{
}

AirTime::~AirTime()
{
}

void AirTime::set_air_msg(AirMsg *air_msg)
{
    m_air_msg = air_msg;
    if (air_msg != nullptr) m_lora_cfg = &air_msg->get_lora_cfg();
}

AirMsg *AirTime::get_air_msg() const
{
    return m_air_msg;
}

void AirTime::set_lora_cfg(LoRaCfg *lora_cfg)
{
    m_lora_cfg = lora_cfg;
}

LoRaCfg *AirTime::get_lora_cfg() const
{
    return m_lora_cfg;
}

double AirTime::get_bandwith()
{
    assert(get_lora_cfg() != nullptr);
    assert(get_lora_cfg()->get_bandwidth() != Bandwidth::NOT_SET);

    switch (get_lora_cfg()->get_bandwidth())
    {
        case Bandwidth::BW125KHZ: return 125000;
        case Bandwidth::BW250KHZ: return 250000;
        case Bandwidth::BW500KHZ: return 500000;
        case Bandwidth::NOT_SET:
        default: return 0;
    }
}

std::size_t AirTime::get_spreading_factor()
{
    assert(get_lora_cfg() != nullptr);
    assert(get_lora_cfg()->get_spreading_factor() != SpreadingFactor::NOT_SET);

    switch (get_lora_cfg()->get_spreading_factor())
    {
        case SpreadingFactor::SF6: return 6;
        case SpreadingFactor::SF7: return 7;
        case SpreadingFactor::SF8: return 8;
        case SpreadingFactor::SF9: return 9;
        case SpreadingFactor::SF10: return 10;
        case SpreadingFactor::SF11: return 11;
        case SpreadingFactor::SF12: return 12;
        case SpreadingFactor::NOT_SET:
        default: return 0;
    }
}

double AirTime::get_symbol_rate()
{
    return get_bandwith() / pow(2.0, get_spreading_factor());
}

double AirTime::get_symbol_period()
{
    return 1 / get_symbol_rate();
}

double AirTime::get_preamble_time()
{
    return get_symbol_period() * get_preamble_size();
}

double AirTime::get_preamble_size()
{
    assert(get_lora_cfg() != nullptr);

    return 4.25 + get_lora_cfg()->get_preamble_size();
}

std::size_t AirTime::get_coding_rate()
{
    assert(get_lora_cfg() != nullptr);
    assert(get_lora_cfg()->get_error_coding_rate() != ErrorCodingRate::NOT_SET);

    switch (get_lora_cfg()->get_error_coding_rate())
    {
        case ErrorCodingRate::EC4_5: return 1;
        case ErrorCodingRate::EC4_6: return 2;
        case ErrorCodingRate::EC4_7: return 3;
        case ErrorCodingRate::EC4_8: return 4;
        case ErrorCodingRate::NOT_SET:
        default: return 0;
    }
}

std::size_t AirTime::get_payload_size(int payload_size)
{
    assert(get_lora_cfg() != nullptr);

    int numerator = (8 * payload_size) - (4 * (int)get_spreading_factor()) + 28;
    if (get_lora_cfg()->get_use_crc()) numerator += 16;
    assert(get_lora_cfg()->get_header_mode() != HeaderMode::NOT_SET);
    if (get_lora_cfg()->get_header_mode() == HeaderMode::IMPLICIT) numerator -= 20;

    std::size_t denominator = 4 * get_spreading_factor();
    if (get_lora_cfg()->get_use_low_data_rate_optimization()) denominator -= 4 * 2;

    int ratio = (int)ceil((double)numerator / (double)denominator);
    int value = ratio * ((int)get_coding_rate() + 4);
    std::size_t count = 8 + (std::size_t)fmax(value, 0);

    return count;
}

std::size_t AirTime::get_payload_size()
{
    return (int)get_air_msg()->get_data().size();
}

double AirTime::get_payload_time()
{
    return get_symbol_period() * get_payload_size();
}

double AirTime::get_payload_time(int payload_size)
{
    return get_symbol_period() * payload_size;
}

double AirTime::get_msg_time()
{
    return get_preamble_time() + get_payload_time();
}

double AirTime::get_msg_time(int payload_size)
{
    int payload_in_symbols = get_payload_size(payload_size);
    return get_preamble_time() + get_payload_time(payload_in_symbols);
}

double AirTime::get_msg_size()
{
    return get_preamble_size() + get_payload_size();
}

double AirTime::get_msg_size(int payload_size)
{
    return get_preamble_size() + get_payload_size(payload_size);
}

int AirTime::max_payload_under_time(double air_time, int max_payload)
{
    double msg_time;

    for (int size = 0; size < max_payload; ++size)
    {
        msg_time = get_msg_time(size);
        if ((size == 0) && (msg_time >= air_time)) return -1;
        if (msg_time >= air_time) return size-1;
    }
    return -2;
}

} // namespace sim

} // namespace esyslora
