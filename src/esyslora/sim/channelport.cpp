/*!
 * \file esyslora/sim/channelport.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/channelport.h"
#include "esyslora/sim/channelmsg.h"

#include <cassert>

namespace esyslora
{

namespace sim
{

ChannelPort::ChannelPort(const sc_module_name &name)
    : sc_module(name)
    , AirMsg_if()
{
}

ChannelPort::~ChannelPort()
{
}

void ChannelPort::am_tx_msg(std::shared_ptr<AirMsg> msg)
{
    std::shared_ptr<ChannelMsg> ch_msg = std::make_shared<ChannelMsg>();

    ch_msg->set_air_msg(msg);
    ch_msg->set_src_port_id(get_id());

    if (get_channel_if() == nullptr) return;

    get_channel_if()->ch_tx_msg(ch_msg);
}

void ChannelPort::am_rx(const LoRaCfg &lora_cfg)
{
    m_lora_cfg = &lora_cfg;
}

void ChannelPort::am_idle()
{
    m_lora_cfg = nullptr;
}

void ChannelPort::chcb_msg_start(std::shared_ptr<ChannelMsg> msg)
{
    if (get_lora_cfg() == nullptr) return;

    bool receivable = m_receivability(*get_lora_cfg(), msg->get_air_msg()->get_lora_cfg());

    if (!receivable) return;

    if (get_cur_msg() == nullptr)
    {
        // No interference
        set_cur_msg(msg);
        return;
    }

    // There are intereferences. For now, assumes that the message is lost
    set_cur_msg(nullptr);
}

void ChannelPort::chcb_msg_done(std::shared_ptr<ChannelMsg> msg)
{
    if (get_cur_msg() == nullptr) return;

    assert(get_cur_msg()->get_id() == msg->get_id());

    if (get_air_msg_cb_if() == nullptr) return;

    get_air_msg_cb_if()->rx_msg(msg->get_air_msg());

    // No pending message.
    set_cur_msg(nullptr);
}

void ChannelPort::set_id(std::size_t id)
{
    m_id = id;
}

std::size_t ChannelPort::get_id() const
{
    return m_id;
}

void ChannelPort::set_channel_if(Channel_if *channel_if)
{
    m_channel_if = channel_if;
}

Channel_if *ChannelPort::get_channel_if() const
{
    return m_channel_if;
}

void ChannelPort::set_air_msg_cb_if(AirMsgCb_if *air_msg_rx_if)
{
    m_air_msg_cb_if = air_msg_rx_if;
}

AirMsgCb_if *ChannelPort::get_air_msg_cb_if() const
{
    return m_air_msg_cb_if;
}

void ChannelPort::set_cur_msg(std::shared_ptr<ChannelMsg> cur_msg)
{
    m_cur_msg = cur_msg;
}

std::shared_ptr<ChannelMsg> ChannelPort::get_cur_msg()
{
    return m_cur_msg;
}

const std::shared_ptr<ChannelMsg> ChannelPort::get_cur_msg() const
{
    return m_cur_msg;
}

Receivability &ChannelPort::get_receivability()
{
    return m_receivability;
}

const LoRaCfg *ChannelPort::get_lora_cfg()
{
    return m_lora_cfg;
}

} // namespace sim

} // namespace esyslora
