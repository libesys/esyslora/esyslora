/*!
 * \file esyslora/sim/sx1272_sim.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/sx1272.h"

#include <cassert>

namespace esyslora
{

namespace sim
{

SX1272::SX1272()
    : Radio()
    , RadioBase()
{
    SC_THREAD(main);

    set_channel_count(1);
}

SX1272::SX1272(const sc_module_name &name)
    : Radio(name)
    , RadioBase()
{
    SC_THREAD(main);

    set_channel_count(1);
}

SX1272::~SX1272()
{
}

void SX1272::set_modulation(Modulation modulation)
{
    m_modulation = modulation;
}

Modulation SX1272::get_modulation() const
{
    return m_modulation;
}

int SX1272::set_lora_cfg(const LoRaCfg &lora_cfg)
{
    assert(get_lora_cfgs().size() == 1);

    get_lora_cfgs()[0] = lora_cfg;

    return 0;
}

const LoRaCfg &SX1272::get_lora_cfg() const
{
    assert(get_lora_cfgs().size() == 1);

    return get_lora_cfgs()[0];
}

LoRaCfg &SX1272::get_lora_cfg()
{
    assert(get_lora_cfgs().size() == 1);

    return get_lora_cfgs()[0];
}

void SX1272::set_tx_power(int8_t tx_power)
{
    m_tx_power = tx_power;
}

int8_t SX1272::get_tx_power()
{
    return m_tx_power;
}

SX1272::Status SX1272::get_status() const
{
    return m_status;
}

void SX1272::set_frequency(double frequency)
{
    m_frequency = frequency;
}

double SX1272::get_frequency() const
{
    return m_frequency;
}

bool SX1272::is_channel_free(int16_t rssi_threshold, uint32_t max_carrier_sense_time)
{
    assert(false);

    return false;
}

int SX1272::tx(uint8_t *packet, uint32_t size)
{
    if (get_status() != Status::IDLE) return -1;

    std::shared_ptr<AirMsg> msg = std::make_shared<AirMsg>();
    std::vector<uint8_t> data;

    data.resize(size);
    memcpy(data.data(), packet, size);

    msg->set_size(size);
    msg->set_data(data);
    msg->set_lora_cfg(get_lora_cfg());

    m_tx_msg = msg;
    m_next_internal_state_evt.cancel();
    m_next_internal_state_evt.notify(m_idle_to_tx);
    set_internal_state(InternalState::GOING_TO_TX);
    return 0;
}

int SX1272::rx(uint32_t timeout)
{
    if (get_status() != Status::IDLE) return -1;

    set_rx_timed_out(false);

    m_rx_timeout = sc_time(timeout, SC_MS);
    set_status(Status::RX_RUNNING);
    m_next_internal_state_evt.notify_delayed();

    auto air_msg_if = get_air_msg_if();
    if (air_msg_if == nullptr) return -2;

    air_msg_if->am_rx(get_lora_cfg());
    return 0;
}

void SX1272::start_channel_activity_detection()
{
}

int16_t SX1272::get_rssi()
{
    assert(false);

    return -1;
}

uint32_t SX1272::get_wake_up_time()
{
    assert(false);

    return 0;
}

void SX1272::set_internal_state(InternalState internal_state)
{
    m_internal_state = internal_state;
}

SX1272::InternalState SX1272::get_internal_state()
{
    return m_internal_state;
}

void SX1272::set_rx_timed_out(bool rx_timed_out)
{
    m_rx_timed_out = rx_timed_out;
}

bool SX1272::get_rx_timed_out() const
{
    return m_rx_timed_out;
}

void SX1272::set_status(Status status)
{
    m_status = status;
}

void SX1272::rx_msg(std::shared_ptr<AirMsg> msg)
{
    if (get_status() != Status::RX_RUNNING) return;

    m_next_internal_state_evt.cancel();

    m_rx_msg = msg;

    set_internal_state(InternalState::GOING_TO_IDLE);
    m_next_internal_state_evt.notify_delayed(m_rx_to_idle);

    if (get_callback() == nullptr) return;

    get_callback()->rx_done(this, m_rx_msg->get_data().data(), m_rx_msg->get_data().size(), m_rx_msg->get_rssi(),
                            m_rx_msg->get_snr());
}

void SX1272::tx_done()
{
    m_next_internal_state_evt.cancel();
    m_next_internal_state_evt.notify(m_tx_to_idle);
    set_internal_state(InternalState::GOING_TX_TO_IDLE);
}

void SX1272::main()
{
    while (true)
    {
        wait(m_next_internal_state_evt);

        switch (get_internal_state())
        {
            case InternalState::IDLE:
                if (get_status() == Status::RX_RUNNING)
                {
                    set_internal_state(InternalState::GOING_TO_RX);
                    m_next_internal_state_evt.notify_delayed(m_idle_to_rx);
                }
                break;
            case InternalState::GOING_TO_IDLE:
                set_internal_state(InternalState::IDLE);
                set_status(Status::IDLE);
                if (get_rx_timed_out() && (get_callback() != nullptr))
                    get_callback()->event(this, RadioCallback::Event::RX_TIMEOUT);
                break;
            case InternalState::GOING_TO_RX:
                set_internal_state(InternalState::RX);
                m_next_internal_state_evt.notify_delayed(m_rx_timeout);
                break;
            case InternalState::RX:
                set_internal_state(InternalState::GOING_TO_IDLE);
                m_next_internal_state_evt.notify_delayed(m_rx_to_idle);
                set_rx_timed_out(true);
                break;
            case InternalState::GOING_TO_TX:
                set_internal_state(InternalState::TX);
                if (get_air_msg_if() != nullptr) get_air_msg_if()->am_tx_msg(m_tx_msg);
                break;
            case InternalState::GOING_TX_TO_IDLE:
                set_internal_state(InternalState::IDLE);
                set_status(Status::IDLE);
                if (get_callback() != nullptr) get_callback()->event(this, RadioCallback::Event::TX_DONE);
                break;
            default:;
        }
    }
}

} // namespace sim

} // namespace esyslora
