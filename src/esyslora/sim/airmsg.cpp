/*!
 * \file esyslora/sim/airmsg.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/airmsg.h"

namespace esyslora
{

namespace sim
{

int64_t AirMsg::m_count = 0;

AirMsg::AirMsg()
    : AirMsgBase()
{
    m_id = m_count;
    ++m_count;

    set_modulation(Modulation::LORA);
}

AirMsg::~AirMsg()
{
}

void AirMsg::set_lora_cfg(const LoRaCfg &lora_cfg)
{
    m_lora_cfg = lora_cfg;
}

const LoRaCfg &AirMsg::get_lora_cfg() const
{
    return m_lora_cfg;
}

LoRaCfg &AirMsg::get_lora_cfg()
{
    return m_lora_cfg;
}

void AirMsg::set_rssi(int16_t rssi)
{
    m_rssi = rssi;
}

int16_t AirMsg::get_rssi() const
{
    return m_rssi;
}

void AirMsg::set_snr(int8_t snr)
{
    m_snr = snr;
}

int8_t AirMsg::get_snr() const
{
    return m_snr;
}

int64_t AirMsg::get_id() const
{
    return m_id;
}

} // namespace sim

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::sim::AirMsg *air_msg)
{
    if (air_msg == nullptr) return os;

    os << "[LoRa packet]" << std::endl;
    os << "    LEN  : " << air_msg->get_size() << std::endl;

    os << (const esyslora::LoRaCfg *)air_msg;

    return os;
}

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::sim::AirMsg &air_msg)
{
    return operator<<(os, &air_msg);
}
