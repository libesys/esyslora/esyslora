/*!
 * \file esyslora/sim/airmsgcb_if.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/airmsgcb_if.h"

namespace esyslora
{

namespace sim
{

AirMsgCb_if::AirMsgCb_if()
{
}

AirMsgCb_if::~AirMsgCb_if()
{
}

} // namespace sim

} // namespace esyslora
