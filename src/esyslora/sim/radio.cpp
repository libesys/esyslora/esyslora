/*!
 * \file esyslora/sim/radio.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/radio.h"

namespace esyslora
{

namespace sim
{

Radio::Radio()
    : sc_module()
{
}

Radio::Radio(const sc_module_name &name)
    : sc_module(name)
{
}

Radio::~Radio()
{
}

void Radio::set_channel_count(std::size_t channel_count)
{
    m_lora_cfgs.resize(channel_count);
}

std::size_t Radio::get_channel_count() const
{
    return m_lora_cfgs.size();
}

std::vector<LoRaCfg> &Radio::get_lora_cfgs()
{
    return m_lora_cfgs;
}

const std::vector<LoRaCfg> &Radio::get_lora_cfgs() const
{
    return m_lora_cfgs;
}

void Radio::set_air_msg_if(AirMsg_if *air_msg_if)
{
    m_air_msg_if = air_msg_if;
}

AirMsg_if *Radio::get_air_msg_if()
{
    return m_air_msg_if;
}

void Radio::rx_msg(std::shared_ptr<AirMsg> msg)
{
}

} // namespace sim

} // namespace esyslora
