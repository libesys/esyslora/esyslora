/*!
 * \file esyslora/sim/airmsgbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/airmsgbase.h"

namespace esyslora
{

namespace sim
{

AirMsgBase::AirMsgBase()
{
}

AirMsgBase::~AirMsgBase()
{
}

void AirMsgBase::set_modulation(Modulation modulation)
{
    m_modulation = modulation;
}

Modulation AirMsgBase::get_modulation() const
{
    return m_modulation;
}

void AirMsgBase::set_data(const std::vector<uint8_t> &data)
{
    m_data = data;
}

const std::vector<uint8_t> &AirMsgBase::get_data() const
{
    return m_data;
}

std::vector<uint8_t> &AirMsgBase::get_data()
{
    return m_data;
}

void AirMsgBase::set_size(std::size_t size)
{
    m_data.resize(size);
}

std::size_t AirMsgBase::get_size() const
{
    return m_data.size();
}

void AirMsgBase::set_sent_time(const sc_time &sent_time)
{
    m_sent_time = sent_time;
}

const sc_time &AirMsgBase::get_sent_time() const
{
    return m_sent_time;
}

} // namespace sim

} // namespace esyslora
