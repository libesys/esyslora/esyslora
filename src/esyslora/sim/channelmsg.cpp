/*!
 * \file esyslora/sim/channelmsg.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/channelmsg.h"

namespace esyslora
{

namespace sim
{

int64_t ChannelMsg::m_count = 0;

ChannelMsg::ChannelMsg()
{
    m_id = m_count;
    ++m_count;
}

ChannelMsg::~ChannelMsg()
{
}

void ChannelMsg::set_air_msg(std::shared_ptr<AirMsg> air_msg)
{
    m_air_msg = air_msg;
}

std::shared_ptr<AirMsg> ChannelMsg::get_air_msg()
{
    return m_air_msg;
}

void ChannelMsg::set_src_port_id(std::size_t src_port_id)
{
    m_src_port_id = src_port_id;
}

std::size_t ChannelMsg::get_src_port_id() const
{
    return m_src_port_id;
}

void ChannelMsg::set_power_level(double power_level)
{
    m_power_level = power_level;
}
double ChannelMsg::get_power_level() const
{
    return m_power_level;
}

void ChannelMsg::set_start_time(const sc_time &start_time)
{
    m_start_time = start_time;
}

const sc_time &ChannelMsg::get_start_time() const
{
    return m_start_time;
}

sc_time &ChannelMsg::get_start_time()
{
    return m_start_time;
}

void ChannelMsg::set_end_time(const sc_time &end_time)
{
    m_end_time = end_time;
}

const sc_time &ChannelMsg::get_end_time() const
{
    return m_end_time;
}

sc_time &ChannelMsg::get_end_time()
{
    return m_end_time;
}

int64_t ChannelMsg::get_id() const
{
    return m_id;
}

} // namespace sim

} // namespace esyslora
