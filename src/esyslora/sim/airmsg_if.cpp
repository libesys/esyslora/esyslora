/*!
 * \file esyslora/sim/airmsg_if.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/airmsg_if.h"

namespace esyslora
{

namespace sim
{

AirMsg_if::AirMsg_if()
{
}

AirMsg_if::~AirMsg_if()
{
}

void AirMsg_if::am_set_cb_if(AirMsgCb_if *rx_if)
{
    m_cb_if = rx_if;
}

AirMsgCb_if *AirMsg_if::am_get_cb_if()
{
    return m_cb_if;
}

} // namespace esyslora

} // namespace esyslora
