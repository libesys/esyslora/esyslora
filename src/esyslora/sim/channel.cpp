/*!
 * \file esyslora/sim/channel.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/channel.h"
#include "esyslora/sim/airtime.h"

#include <sstream>

namespace esyslora
{

namespace sim
{

Channel::Channel(const sc_module_name &name)
    : sc_module(name)
{
    SC_THREAD(main);
}

Channel::~Channel()
{
}

void Channel::add(Radio *radio)
{
    std::ostringstream oss;

    oss << "ChPort" << m_ports.size();

    std::shared_ptr<ChannelPort> port = std::make_shared<ChannelPort>(oss.str().c_str());

    port->set_id(m_ports.size());
    radio->set_air_msg_if(port.get());
    port->am_set_cb_if(radio);

    port->set_channel_if(this);
    port->set_air_msg_cb_if(radio);

    m_ports.push_back(port);
}

std::vector<std::shared_ptr<ChannelPort>> &Channel::get_ports()
{
    return m_ports;
}

const std::vector<std::shared_ptr<ChannelPort>> &Channel::get_ports() const
{
    return m_ports;
}

void Channel::ch_tx_msg(std::shared_ptr<ChannelMsg> msg)
{
    AirTime air_time;

    air_time.set_air_msg(msg->get_air_msg().get());

    double msg_time = air_time.get_msg_time();
    sc_time msg_sc_time = sc_time(msg_time, SC_SEC);

    msg->set_start_time(sc_time_stamp());
    msg->set_end_time(sc_time_stamp() + msg_sc_time);

    m_msg_queue.add(msg);

    for (std::size_t idx = 0; idx < m_ports.size(); ++idx)
    {
        if (idx != msg->get_src_port_id()) m_ports[idx]->chcb_msg_start(msg);
    }
}

void Channel::main()
{
    std::size_t queue_size;
    sc_time evt_time;

    while (1)
    {
        wait(m_msg_queue.get_event());

        queue_size = m_msg_queue.size();
        if (queue_size == 0) continue;

        evt_time = m_msg_queue[0].m_msg->get_end_time();

        while ((m_msg_queue.size() > 0) && (evt_time == m_msg_queue[0].m_msg->get_end_time()))
        {
            auto msg = m_msg_queue[0].m_msg;
            auto src_port_id = msg->get_src_port_id();

            for (std::size_t idx = 0; idx < m_ports.size(); ++idx)
            {
                if (idx != src_port_id) m_ports[idx]->chcb_msg_done(msg);
            }

            auto air_msg_rx_if = m_ports[src_port_id]->get_air_msg_cb_if();
            if (air_msg_rx_if != nullptr) air_msg_rx_if->tx_done();

            m_msg_queue.pop();
        }

        m_msg_queue.update_event();
    }
}

} // namespace sim

} // namespace esyslora
