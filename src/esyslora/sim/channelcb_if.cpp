/*!
 * \file esyslora/sim/channelcb_if.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/sim/channelcb_if.h"

namespace esyslora
{

namespace sim
{

ChannelCb_if::ChannelCb_if()
{
}

ChannelCb_if::~ChannelCb_if()
{
}

} // namespace sim

} // namespace esyslora
