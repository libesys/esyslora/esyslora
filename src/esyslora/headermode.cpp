/*!
 * \file esyslora/headermode.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/headermode.h"

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::HeaderMode header_mode)
{
    switch (header_mode)
    {
        case esyslora::HeaderMode::EXPLICIT: os << "explicit"; break;
        case esyslora::HeaderMode::IMPLICIT: os << "implicit"; break;
        case esyslora::HeaderMode::NOT_SET:
        default: os << "NOT SET";
    };
    return os;
}
