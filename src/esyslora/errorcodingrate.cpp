/*!
 * \file esyslora/errorcodingrate.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/errorcodingrate.h"

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::ErrorCodingRate error_coding_rate)
{
    switch (error_coding_rate)
    {
        case esyslora::ErrorCodingRate::EC4_5: os << "4/5 (1)"; break;
        case esyslora::ErrorCodingRate::EC4_6: os << "4/6 (2)"; break;
        case esyslora::ErrorCodingRate::EC4_7: os << "4/7 (3)"; break;
        case esyslora::ErrorCodingRate::EC4_8: os << "4/8 (4)"; break;
        case esyslora::ErrorCodingRate::NOT_SET:
        default: os << "NOT SET";
    };
    return os;
}