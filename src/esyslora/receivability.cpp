/*!
 * \file esyslora/receivability.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/receivability.h"

namespace esyslora
{

Receivability::Receivability()
{
}

void Receivability::set_frequency_variation(double frequency_variation)
{
    m_frequency_variation = frequency_variation;
}

double Receivability::get_frequency_variation()
{
    return m_frequency_variation;
}

bool Receivability::operator()(const LoRaCfg &receiver_cfg, const LoRaCfg &recv_msg_cfg)
{
    double max_freq = receiver_cfg.get_frequency() + get_frequency_variation() / 2.0;
    double min_freq = receiver_cfg.get_frequency() - get_frequency_variation() / 2.0;

    if (receiver_cfg.get_frequency() > max_freq) return false;
    if (receiver_cfg.get_frequency() < min_freq) return false;

    if (receiver_cfg.get_spreading_factor() != recv_msg_cfg.get_spreading_factor()) return false;
    if (receiver_cfg.get_error_coding_rate() != recv_msg_cfg.get_error_coding_rate()) return false;
    if (receiver_cfg.get_bandwidth() != recv_msg_cfg.get_bandwidth()) return false;
    if (receiver_cfg.get_preamble_size() != recv_msg_cfg.get_preamble_size()) return false;

    return true;
}

} // namespace esyslora
