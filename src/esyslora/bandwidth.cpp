/*!
 * \file esyslora/bandwidth.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/bandwidth.h"

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::Bandwidth bandwidth)
{
    switch (bandwidth)
    {
        case esyslora::Bandwidth::BW125KHZ: os << "125 khz"; break;
        case esyslora::Bandwidth::BW250KHZ: os << "250 khz"; break;
        case esyslora::Bandwidth::BW500KHZ: os << "500 khz"; break;
        case esyslora::Bandwidth::NOT_SET:
        default: os << " NOT SET";
    };
    return os;
}
