/*!
 * \file esyslora/loracf.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/loracfg.h"

namespace esyslora
{

LoRaCfg::LoRaCfg()
{
}

LoRaCfg::LoRaCfg(Bandwidth bandwidth, ErrorCodingRate error_coding_rate, SpreadingFactor spreading_factor)
    : m_bandwidth(bandwidth)
    , m_error_coding_rate(error_coding_rate)
    , m_spreading_factor(spreading_factor)
{
    
}

LoRaCfg::~LoRaCfg()
{
}

void LoRaCfg::set_bandwidth(Bandwidth bandwidth)
{
    m_bandwidth = bandwidth;
}

Bandwidth LoRaCfg::get_bandwidth() const
{
    return m_bandwidth;
}

void LoRaCfg::set_error_coding_rate(ErrorCodingRate error_coding_rate)
{
    m_error_coding_rate = error_coding_rate;
}

ErrorCodingRate LoRaCfg::get_error_coding_rate() const
{
    return m_error_coding_rate;
}

void LoRaCfg::set_header_mode(HeaderMode header_mode)
{
    m_header_mode = header_mode;
}

HeaderMode LoRaCfg::get_header_mode() const
{
    return m_header_mode;
}

void LoRaCfg::set_spreading_factor(SpreadingFactor spreading_factor)
{
    m_spreading_factor = spreading_factor;
}

SpreadingFactor LoRaCfg::get_spreading_factor() const
{
    return m_spreading_factor;
}

void LoRaCfg::set_preamble_size(std::size_t preamble_size)
{
    m_preamble_size = preamble_size;
}

std::size_t LoRaCfg::get_preamble_size() const
{
    return m_preamble_size;
}

void LoRaCfg::set_use_crc(bool use_crc)
{
    m_use_crc = use_crc;
}

bool LoRaCfg::get_use_crc() const
{
    return m_use_crc;
}

void LoRaCfg::set_use_low_data_rate_optimization(bool use_low_data_rate_optimization)
{
    m_use_low_data_rate_optimization = use_low_data_rate_optimization;
}

bool LoRaCfg::get_use_low_data_rate_optimization() const
{
    return m_use_low_data_rate_optimization;
}

void LoRaCfg::set_frequency(double frequency)
{
    m_frequency = frequency;
}

double LoRaCfg::get_frequency() const
{
    return m_frequency;
}

} // namespace esyslora

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::LoRaCfg *air_msg)
{
    if (air_msg == nullptr) return os;

    os << "    SF   : " << air_msg->get_spreading_factor() << std::endl;
    os << "    BW   : " << air_msg->get_bandwidth() << std::endl;
    os << "    CR   : " << air_msg->get_error_coding_rate() << std::endl;
    os << "    PL   : " << air_msg->get_preamble_size() << " symbols" << std::endl;
    os << "    HDR  : " << air_msg->get_header_mode() << std::endl;
    os << "    CRC  : " << air_msg->get_use_crc() << std::endl;
    os << "    LDRO : " << air_msg->get_use_low_data_rate_optimization() << std::endl;
    os << "    FREQ : " << air_msg->get_frequency() << " MHZ" << std::endl;

    return os;
}

ESYSLORA_API std::ostream &operator<<(std::ostream &os, const esyslora::LoRaCfg &air_msg)
{
    return operator<<(os, &air_msg);
}
