/*!
 * \file esyslora/modulation.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esyslora/esyslora_prec.h"
#include "esyslora/modulation.h"

ESYSLORA_API std::ostream &operator<<(std::ostream &os, esyslora::Modulation modulation)
{
    switch (modulation)
    {
        case esyslora::Modulation::LORA: os << "LoRa"; break;
        case esyslora::Modulation::FSK_OOK: os << "FSK/OOK"; break;
        case esyslora::Modulation::NOT_SET:
        default:  os << "NOT SET";
    };
    return os;
}

