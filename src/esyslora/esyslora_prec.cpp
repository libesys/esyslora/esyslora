/*!
 * \file esyslora/esyslora_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

// esyslora.cpp : source file that includes just the standard includes
// esyslora.pch will be the pre-compiled header
// esyslora.obj will contain the pre-compiled type information

#include "esyslora/esyslora_prec.h"

// TODO: reference any additional headers you need in esyslora_prec.h
// and not in this file
